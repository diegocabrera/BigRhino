<!DOCTYPE html>
<!--[if IE 8]>    <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>    <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--><html lang="es"><!--<![endif]-->
<head>
	<?php echo $__env->make('pagina::partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head><!--/head-->

<body class="page-container-bg-solid">
<div class="row">
	<div class="col-md-12">
	    <?php echo $__env->yieldContent('content-top'); ?>
		<?php echo $__env->yieldContent('content'); ?>
	</div>
</div>
<div class="row">
    <?php echo $__env->make('pagina::partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
</body>
</html>
