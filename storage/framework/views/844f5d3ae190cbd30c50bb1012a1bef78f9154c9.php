<?php $__env->startSection('content-top'); ?>
    <?php echo $__env->make('base::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('base::partials.ubicacion', ['ubicacion' => ['Eventos']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Eventos.',
        'columnas' => [
            'Title' => '33.333333333333',
		'Lugar' => '33.333333333333',
		'Image' => '33.333333333333'
        ]
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]); ?>

        <?php echo e(Form::bsText('title', '', [
            'label' => 'Titulo',
            'placeholder' => 'Titulo',
            'required' => 'required'
        ])); ?>

        <?php echo e(Form::bsText('lugar', '', [
            'label' => 'Lugar',
            'placeholder' => 'Lugar',
            'required' => 'required'
        ])); ?>

        <div class="">
            <label for="">Imagen</label>
            <br>
            <span class="btn btn-success fileinput-button">
                <i class="fa fa-plus"></i>
                <span>Agregar Archivo...</span>
                <input type="file" name="image">
            </span>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make(isset($layouts) ? $layouts : 'base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>