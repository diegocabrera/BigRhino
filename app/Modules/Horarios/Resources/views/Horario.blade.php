@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Horario']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Horario.',
        'columnas' => [
            'Ejercicio' => '20',
		'Dia' => '20',
		'Hora' => '20',
		'Detalles' => '20',
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Horario->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection