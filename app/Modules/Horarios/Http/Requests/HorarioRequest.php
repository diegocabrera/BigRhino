<?php

namespace App\Modules\Horarios\Http\Requests;

use App\Http\Requests\Request;

class HorarioRequest extends Request {
    protected $reglasArr = [
		'ejercicio' => ['required', 'min:3', 'max:250'], 
		'dia' => ['required', 'min:3', 'max:250'], 
		'hora' => ['string', 'min:3', 'max:250'], 
		'detalles' => ['required']
	];
}