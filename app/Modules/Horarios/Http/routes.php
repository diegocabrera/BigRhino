<?php

Route::group(['middleware' => 'web', 'prefix' => 'horarios', 'namespace' => 'App\\Modules\Horarios\Http\Controllers'], function()
{
    Route::get('/',                     'HorarioController@index');
    Route::post('guardar',              'HorarioController@guardar');
    Route::get('buscar/{id}',           'HorarioController@buscar');
    Route::put('guardar/{id}',          'HorarioController@guardar');
    Route::get('datatable',             'HorarioController@datatable');
    Route::delete('eliminar/{id}',      'HorarioController@eliminar');
    Route::post('restaurar/{id}',       'HorarioController@restaurar');
    Route::delete('destruir/{id}',      'HorarioController@destruir');
});