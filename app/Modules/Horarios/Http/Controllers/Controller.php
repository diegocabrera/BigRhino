<?php

namespace App\Modules\Horarios\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'base';
	// public $autenticar = false;
	protected $titulo = 'Horarios';
	// public $prefijo_ruta = 'backend';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'App/Modules/Horarios/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'App/Modules/Horarios/Assets/css',
	];

}
