<?php

namespace App\Modules\Horarios\Http\Controllers;

//Controlador Padre
use App\Modules\Horarios\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Horarios\Http\Requests\HorarioRequest;

//Modelos
use App\Modules\Horarios\Models\Horario;

class HorarioController extends Controller
{
    protected $titulo = 'Horario';

    public $js = [
        'Horario'
    ];
    
    public $css = [
        'Horario'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('horarios::Horario', [
            'Horario' => new Horario()
        ]);
    }

    public function nuevo()
    {
        $Horario = new Horario();
        return $this->view('horarios::Horario', [
            'layouts' => 'base::layouts.popup',
            'Horario' => $Horario
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Horario = Horario::find($id);
        return $this->view('horarios::Horario', [
            'layouts' => 'base::layouts.popup',
            'Horario' => $Horario
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Horario = Horario::withTrashed()->find($id);
        } else {
            $Horario = Horario::find($id);
        }

        if ($Horario) {
            return array_merge($Horario->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(HorarioRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Horario = $id == 0 ? new Horario() : Horario::find($id);

            $Horario->fill($request->all());
            $Horario->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Horario->id,
            'texto' => $Horario->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Horario::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Horario::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Horario::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Horario::select([
            'id', 'ejercicio', 'dia', 'hora', 'detalles', 'published_at', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}