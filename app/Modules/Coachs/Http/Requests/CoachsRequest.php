<?php

namespace App\Modules\Coachs\Http\Requests;

use App\Http\Requests\Request;

class CoachsRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:250'], 
		'edad' => ['required', 'integer'], 
		'detalles' => ['required']
	];
}