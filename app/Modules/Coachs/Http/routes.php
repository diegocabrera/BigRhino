<?php

Route::group(['middleware' => 'web', 'prefix' => 'coachs', 'namespace' => 'App\\Modules\Coachs\Http\Controllers'], function()
{
    Route::get('/',                     'CoachsController@index');
    Route::post('guardar',              'CoachsController@guardar');
    Route::get('buscar/{id}',           'CoachsController@buscar');
    Route::put('guardar/{id}',          'CoachsController@guardar');
    Route::get('datatable',             'CoachsController@datatable');
    Route::delete('eliminar/{id}',      'CoachsController@eliminar');
    Route::post('restaurar/{id}',       'CoachsController@restaurar');
    Route::delete('destruir/{id}',      'CoachsController@destruir');
});
