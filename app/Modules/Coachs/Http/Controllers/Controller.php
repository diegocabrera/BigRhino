<?php

namespace App\Modules\Coachs\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'base';
	// public $autenticar = false;
	protected $titulo = 'Coachs';
	// public $prefijo_ruta = 'backend';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'App/Modules/Coachs/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'App/Modules/Coachs/Assets/css',
	];

}
