<?php

namespace App\Modules\Coachs\Models;

use App\Modules\base\Models\Modelo;



class Coachs extends modelo
{
    protected $table = 'coachs';
    protected $fillable = ["nombre","edad","detalles"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Coachs'
    ],
    'edad' => [
        'type' => 'number',
        'label' => 'Edad',
        'placeholder' => 'Edad del Coachs'
    ],
    'detalles' => [
        'type' => 'textarea',
        'label' => 'Detalles',
        'placeholder' => 'Detalles del Coachs'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}