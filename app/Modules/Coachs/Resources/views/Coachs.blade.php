@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Coachs']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Coachs.',
        'columnas' => [
            'Nombre' => '33.333333333333',
		'Edad' => '33.333333333333',
		'Detalles' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Coachs->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection