<?php

namespace App\Modules\Noticias\Http\Controllers;
//controlador papito
use App\Modules\Noticias\Http\Controllers\Controller;
//Dependencias
use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Datatables;
//Request
use App\Modules\Noticias\Http\Request\CategoriasRequest;
//Modelooos
use App\Modules\Noticias\Models\Categorias;


class CategoriasController extends Controller
{
    protected $titulo = 'Categorías';
    public $js =[
        'Categorias'
    ];
    public $css =[

    ];
    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'bootstrap-colorpicker'
    ];

    public function index(){
        return $this->view('noticias::Categorias', [
            'Categorias'=> new Categorias()
        ]);
    }

    public function buscar(Request $request, $id = 0) {
		if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')){
			$categoria = Categorias::withTrashed()->find($id);
		}else{
			$categoria = Categorias::find($id);
		}

		if ($categoria) {
			$respuesta = array_merge($categoria->toArray(), [
				's' => 's',
				'msj' => trans('controller.buscar'),
			]);

			return $respuesta;
		}

		return trans('controller.nobuscar');
	}

    public function guardar(Request $request, $id = 0) {
		try {
			if ($id === 0){
				Categorias::create($request->all());
			}else{
				Categorias::find($id)->update($request->all());
			}
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

    public function eliminar(Request $request, $id = 0) {
		try {
			$usuario = Categorias::destroy($id);
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

    public function restaurar(Request $request, $id = 0) {
		try {
			Categorias::withTrashed()->find($id)->restore();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.restaurar')];
	}

    public function destruir(Request $request, $id = 0) {
		try {
			Categorias::withTrashed()->find($id)->forceDelete();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.destruir')];
	}

    public function datatable(Request $request) {
		$sql = Categorias::select([
			'id', 'nombre', 'descripcion',
		]);

		return Datatables::of($sql)->setRowId('id')->make(true);
	}
}
